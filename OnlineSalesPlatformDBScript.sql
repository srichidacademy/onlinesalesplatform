USE [OnlineSalesPlatformAug16Batch]
GO
/****** Object:  Synonym [dbo].[sy_dummyProducts]    Script Date: 06-Sep-21 7:37:14 PM ******/
CREATE SYNONYM [dbo].[sy_dummyProducts] FOR [OnlineSalesPlatformAug16Batch].[dbo].[tbl_Products]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFullName]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create function [dbo].[fn_GetFullName](
@firstName nvarchar(100),
@lastName nvarchar(100)
)
returns nvarchar(200)
as
begin
return (@firstName +' '+@lastName)
end
GO
/****** Object:  Table [dbo].[tbl_ProductCategory]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_ProductCategory](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_tbl_ProductCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Products]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](200) NULL,
	[Category] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[Price] [decimal](18, 2) NULL,
	[StockAvailable] [int] NULL,
	[ProductImage] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_Products] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vw_GetProductsStartsFromLowPrice]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_GetProductsStartsFromLowPrice]
AS
SELECT        TOP (100) PERCENT T1.ProductId, T1.ProductName, T2.CategoryName, T1.Price, T1.StockAvailable
FROM            dbo.tbl_Products AS T1 INNER JOIN
                         dbo.tbl_ProductCategory AS T2 ON T1.Category = T2.CategoryId
GO
/****** Object:  Table [dbo].[tbl_CustomerCart]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerCart](
	[CartId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[AddedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_CustomerCart] PRIMARY KEY CLUSTERED 
(
	[CartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerProfile]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerProfile](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](200) NOT NULL,
	[LastName] [nvarchar](200) NOT NULL,
	[DOB] [date] NULL,
	[Gender] [varchar](15) NULL,
	[Password] [varchar](200) NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[PhoneNumber] [nvarchar](15) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_CustomerProfile] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_CustomerTransactions]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_CustomerTransactions](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Qty] [int] NOT NULL,
	[TransactionAmount] [decimal](18, 2) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionStatus] [bit] NOT NULL,
	[PaymentMode] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_tbl_CustomerTransactions] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_CustomerProfile] ON 
GO
INSERT [dbo].[tbl_CustomerProfile] ([CustomerId], [FirstName], [LastName], [DOB], [Gender], [Password], [Email], [PhoneNumber], [IsActive], [CreatedOn]) VALUES (1, N'Gangireddy', N'Gubala', CAST(N'1989-05-08' AS Date), N'Male', N'Srichid@123', N'gangireddy@srichidtechnologies.com', N'8884118489', 1, CAST(N'2021-09-01T08:55:43.230' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_CustomerProfile] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_CustomerTransactions] ON 
GO
INSERT [dbo].[tbl_CustomerTransactions] ([TransactionId], [ProductId], [CustomerId], [Qty], [TransactionAmount], [TransactionDate], [TransactionStatus], [PaymentMode]) VALUES (1, 1, 1, 2, CAST(60000.00 AS Decimal(18, 2)), CAST(N'2021-09-01T19:39:08.653' AS DateTime), 1, N'PAYTM')
GO
SET IDENTITY_INSERT [dbo].[tbl_CustomerTransactions] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_ProductCategory] ON 
GO
INSERT [dbo].[tbl_ProductCategory] ([CategoryId], [CategoryName], [IsActive]) VALUES (1, N'Mobile', 1)
GO
INSERT [dbo].[tbl_ProductCategory] ([CategoryId], [CategoryName], [IsActive]) VALUES (2, N'Computer', 1)
GO
INSERT [dbo].[tbl_ProductCategory] ([CategoryId], [CategoryName], [IsActive]) VALUES (3, N'Watch', 1)
GO
SET IDENTITY_INSERT [dbo].[tbl_ProductCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Products] ON 
GO
INSERT [dbo].[tbl_Products] ([ProductId], [ProductName], [Category], [Description], [Price], [StockAvailable], [ProductImage], [IsActive], [CreatedOn]) VALUES (1, N'SAMSUNG S20', 1, N'Samsungs latest product with high volume camera and water proof', CAST(50000.00 AS Decimal(18, 2)), 8, N'', 1, CAST(N'2021-08-31T08:29:44.807' AS DateTime))
GO
INSERT [dbo].[tbl_Products] ([ProductId], [ProductName], [Category], [Description], [Price], [StockAvailable], [ProductImage], [IsActive], [CreatedOn]) VALUES (2, N'Redmi Note Pro 10', 1, N'Redmi Current trend product', CAST(20000.00 AS Decimal(18, 2)), 19, N'', 1, CAST(N'2021-08-31T08:31:23.470' AS DateTime))
GO
INSERT [dbo].[tbl_Products] ([ProductId], [ProductName], [Category], [Description], [Price], [StockAvailable], [ProductImage], [IsActive], [CreatedOn]) VALUES (3, N'ASUS XFF', 2, N'Good Computer', CAST(56000.00 AS Decimal(18, 2)), 4, N'', 1, CAST(N'2021-08-31T08:32:03.620' AS DateTime))
GO
INSERT [dbo].[tbl_Products] ([ProductId], [ProductName], [Category], [Description], [Price], [StockAvailable], [ProductImage], [IsActive], [CreatedOn]) VALUES (4, N'DELL 67', 2, N'Good Computer', CAST(48000.00 AS Decimal(18, 2)), 8, N'', 1, CAST(N'2021-08-31T08:32:41.463' AS DateTime))
GO
INSERT [dbo].[tbl_Products] ([ProductId], [ProductName], [Category], [Description], [Price], [StockAvailable], [ProductImage], [IsActive], [CreatedOn]) VALUES (5, N'OPPO MOB123', 1, N'Latest product in OPPO', CAST(30000.00 AS Decimal(18, 2)), 2, N'', 1, CAST(N'2021-08-31T19:51:24.150' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tbl_Products] OFF
GO
ALTER TABLE [dbo].[tbl_CustomerCart] ADD  CONSTRAINT [DF_tbl_CustomerCart_AddedOn]  DEFAULT (getdate()) FOR [AddedOn]
GO
ALTER TABLE [dbo].[tbl_CustomerProfile] ADD  CONSTRAINT [DF_tbl_CustomerProfile_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tbl_CustomerProfile] ADD  CONSTRAINT [DF_tbl_CustomerProfile_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tbl_CustomerTransactions] ADD  CONSTRAINT [DF_tbl_CustomerTransactions_TransactionDate]  DEFAULT (getdate()) FOR [TransactionDate]
GO
ALTER TABLE [dbo].[tbl_CustomerTransactions] ADD  CONSTRAINT [DF_tbl_CustomerTransactions_TransactionStatus]  DEFAULT ((0)) FOR [TransactionStatus]
GO
ALTER TABLE [dbo].[tbl_ProductCategory] ADD  CONSTRAINT [DF_tbl_ProductCategory_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tbl_Products] ADD  CONSTRAINT [DF_tbl_Products_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tbl_Products] ADD  CONSTRAINT [DF_tbl_Products_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[tbl_CustomerCart]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCart_tbl_CustomerProfile] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_CustomerProfile] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerCart] CHECK CONSTRAINT [FK_tbl_CustomerCart_tbl_CustomerProfile]
GO
ALTER TABLE [dbo].[tbl_CustomerCart]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerCart_tbl_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[tbl_Products] ([ProductId])
GO
ALTER TABLE [dbo].[tbl_CustomerCart] CHECK CONSTRAINT [FK_tbl_CustomerCart_tbl_Products]
GO
ALTER TABLE [dbo].[tbl_CustomerTransactions]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerTransactions_tbl_CustomerProfile] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[tbl_CustomerProfile] ([CustomerId])
GO
ALTER TABLE [dbo].[tbl_CustomerTransactions] CHECK CONSTRAINT [FK_tbl_CustomerTransactions_tbl_CustomerProfile]
GO
ALTER TABLE [dbo].[tbl_CustomerTransactions]  WITH CHECK ADD  CONSTRAINT [FK_tbl_CustomerTransactions_tbl_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[tbl_Products] ([ProductId])
GO
ALTER TABLE [dbo].[tbl_CustomerTransactions] CHECK CONSTRAINT [FK_tbl_CustomerTransactions_tbl_Products]
GO
ALTER TABLE [dbo].[tbl_Products]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Products_tbl_ProductCategory] FOREIGN KEY([Category])
REFERENCES [dbo].[tbl_ProductCategory] ([CategoryId])
GO
ALTER TABLE [dbo].[tbl_Products] CHECK CONSTRAINT [FK_tbl_Products_tbl_ProductCategory]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllProducts]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Reddy
-- Create date: 31st Aug 2021
-- Description:	Get All Products
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAllProducts] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select T1.ProductId,T1.ProductName , T2.CategoryName , T1.Price , T1.StockAvailable  from tbl_Products T1 inner join tbl_ProductCategory T2 on T1.Category =t2.CategoryId 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_GetProductDetails]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[sp_GetProductDetails]
(@productId int)
As
Begin
  select * from tbl_Products where ProductId =@productId
End
GO
/****** Object:  StoredProcedure [dbo].[sp_SaveProduct]    Script Date: 06-Sep-21 7:37:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[sp_SaveProduct]
(@productName nvarchar(200),
 @category int,
 @description nvarchar(500),
 @price decimal(18,2),
 @stockavailable int,
 @productImage nvarchar(max)
)
As
BEGIN
  Insert into tbl_Products (
  ProductName,
Category,
Description,
Price,
StockAvailable,
ProductImage
) values(

@productName ,
@category ,
@description ,
@price ,
@stockavailable ,
@productImage 
)
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "T2"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "T1"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetProductsStartsFromLowPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_GetProductsStartsFromLowPrice'
GO
USE [master]
GO
ALTER DATABASE [OnlineSalesPlatformAug16Batch] SET  READ_WRITE 
GO
