import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedserviceService {

  private productSource = new BehaviorSubject<string>('');
  productName = this.productSource.asObservable()
  
  constructor() { }
  
  changeProduct(productName: string) {
    this.productSource.next(productName);
  }
}
