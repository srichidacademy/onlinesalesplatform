import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
newlyAddedProduct:string;
  constructor(private route:Router,private sharedService:SharedserviceService) { }
 searchString:string;
  products:Product[]=[
    {ProductId:1,ProductName:'SamSung SX3', Description:'It is new arrival in Samsung',Price:20000,StockAvailability:true,NoofProductsAvailable:5,Reviews:'It is good Product', Category:'Mobile',Image:'mobile1.jpg'}
  ,{ProductId:2,ProductName:'Titan Tx5', Description:'It is new arrival in Tatan',Price:10000,StockAvailability:true,NoofProductsAvailable:20,Reviews:'It is good Product', Category:'Watch',Image:'w.jpg'}
,{ProductId:3,ProductName:'DELL 64Bit', Description:'DELL Product',Price:75000,StockAvailability:false,NoofProductsAvailable:0,Reviews:'It is good Product', Category:'Computer',Image:'mobile1.jpg'}  
,{ProductId:4,ProductName:'OPPO O9', Description:'It is new arrival in OPPO',Price:30000,StockAvailability:true,NoofProductsAvailable:20,Reviews:'It is good Product', Category:'Mobile',Image:'mobile1.jpg'}
,{ProductId:5,ProductName:'HP XDS', Description:'Best Computer in HP',Price:65000,StockAvailability:true,NoofProductsAvailable:1,Reviews:'It is good Product', Category:'Computer',Image:'mobile1.jpg'}
];
  

  ngOnInit(): void {
    
    this.sharedService.productName.subscribe(result => {
      this.newlyAddedProduct = result; // this set's the productname to the default observable value
    });
  }

  NavigateToAddproduct(){
     this.route.navigate(['addproduct']);
  }

}

class Product{
  ProductId:number;
  ProductName:string;
  Description:string;
  Price:number;
  StockAvailability:boolean;
  NoofProductsAvailable:number;
  Reviews:string;
  Category:string;
  Image:string;

}
