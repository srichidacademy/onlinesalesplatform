import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedserviceService } from '../services/sharedservice.service';

@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
productName:string;
  constructor(private sharedService:SharedserviceService, private route:Router) { }

  productProfile=new FormGroup({    
    productName:new FormControl("productName"),
    category:new FormControl("category"),
    description:new FormControl("description"),
    price:new FormControl("price"),
    stockAvailability:new FormControl("stockAvailability"),
    reviews:new FormControl("reviews")
    
  })
      
  ngOnInit(): void {
    this.sharedService.productName.subscribe(result => {
      this.productName = result; // this set's the productname to the default observable value
    });
  }



  onSubmit(){
    this.sharedService.changeProduct(this.productProfile.value["productName"]);
this.route.navigate(['products']);
    // alert("New Product Details are : the ProductName: "+this.productProfile.value["productName"]+"");
  }

}
