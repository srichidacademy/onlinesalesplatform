import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ContactusComponent } from './contactus/contactus.component';
import { CustomersComponent } from './customers/customers.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ProductsComponent } from './products/products.component';
import { RegistrationComponent } from './registration/registration.component';

const routes: Routes = [
   {path:'home', component:HomepageComponent},
   {path:'aboutus', component:AboutusComponent},
   {path:'contactus', component:ContactusComponent}, 
   {path:'registration', component:RegistrationComponent},
   {path:'customers', component:CustomersComponent},
   {path:'products', component:ProductsComponent},
   {path:'addproduct', component:AddproductComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
